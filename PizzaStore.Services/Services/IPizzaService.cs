﻿using System.Collections.Generic;
using PizzaStore.DAL.Models;

namespace PizzaStore.Services.Service
{
    public interface IPizzaService
    {
        Pizza GetPizzaById(string id);
        List<Pizza> GetPizzas();
    }
}