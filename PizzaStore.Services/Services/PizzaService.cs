﻿using PizzaStore.DAL.Models;
using PizzaStore.Repository.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace PizzaStore.Services.Service
{
    public class PizzaService : IPizzaService
    {
        readonly IRepository<Pizza> _repository;

        public PizzaService()
        {
            _repository = new Repository<Pizza>();
        }

        public Pizza GetPizzaById(string id)
        {
            return _repository.Get(x => x.Id == id);
        }

        public List<Pizza> GetPizzas()
        {
            return _repository.GetAll().ToList();
        }
    }
}
