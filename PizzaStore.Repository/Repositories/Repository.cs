﻿using PizzaStore.DAL.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PizzaStore.Repository.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        readonly IDataAccess<T> _context;

        public Repository()
        {
            _context = new DataAccess<T>();
        }

        public T Get(Func<T, bool> predicate)
        {
            return _context.Collection.FirstOrDefault(predicate);
        }

        public IEnumerable<T> GetAll()
        {
            return _context.Collection;
        }
    }
}
