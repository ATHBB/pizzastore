﻿using System;
using System.Collections.Generic;

namespace PizzaStore.Repository.Repositories
{
    public interface IRepository<T> where T : class
    {
        T Get(Func<T, bool> predicate);
        IEnumerable<T> GetAll();
    }
}