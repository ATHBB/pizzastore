﻿using PizzaStore.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PizzaStore.Responsobilty.Successors
{
    public class PLSuccessor : BaseSuccessor
    {
        public override Pizza GetPizza(Pizza pizza, string translateCode)
        {
            var PizzaContnent = pizza.Contents.FirstOrDefault(x => x.LanguageCode.Equals("pl-PL", StringComparison.InvariantCultureIgnoreCase));
            if (PizzaContnent != null)
                return new Pizza { Price = pizza.Price, Contents = new List<PizzaContent> { new PizzaContent { Name = PizzaContnent.Name } } };
            return _successor.GetPizza(pizza, "");
        }

        public override List<Pizza> GetPizzas(List<Pizza> pizzaList, string translateCode)
        {
            if (pizzaList.Any(x => x.Contents.Any(z => z.LanguageCode == translateCode)))
                return pizzaList.Where(x => x.Contents.Any(z => z.LanguageCode.Equals("pl-PL", StringComparison.InvariantCultureIgnoreCase))).ToList().Select(x => new Pizza { Price = x.Price, Contents = new List<PizzaContent> { new PizzaContent { Name = x.Contents.FirstOrDefault(z => z.LanguageCode.Equals("pl-PL", StringComparison.InvariantCultureIgnoreCase)).Name } } }).ToList();
            return _successor.GetPizzas(pizzaList, "");
        }
    }
}
