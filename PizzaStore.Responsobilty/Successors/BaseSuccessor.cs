﻿using PizzaStore.DAL.Models;
using System.Collections.Generic;

namespace PizzaStore.Responsobilty.Successors
{
    public abstract class BaseSuccessor
    {
        protected BaseSuccessor _successor;

        public void SetSuccessor(BaseSuccessor successor)
        {
            _successor = successor;
        }

        public abstract Pizza GetPizza(Pizza pizza, string translateCode);

        public abstract List<Pizza> GetPizzas(List<Pizza> pizzaList, string translateCode);
    }
}
