﻿using System.Collections.Generic;
using System.Linq;
using PizzaStore.DAL.Models;

namespace PizzaStore.Responsobilty.Successors
{
    public class AnySuccessor : BaseSuccessor
    {
        public override Pizza GetPizza(Pizza pizza, string translateCode)
        {
            return new Pizza { Price = pizza.Price, Contents = new List<PizzaContent> { new PizzaContent { Name = pizza.Contents.First().Name } } };
        }

        public override List<Pizza> GetPizzas(List<Pizza> pizzaList, string translateCode)
        {
            return pizzaList;
        }
    }
}
