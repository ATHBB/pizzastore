﻿using PizzaStore.Application;
using System;
using System.Web.Http;

namespace PizzaStore.Controllers
{
    [RoutePrefix("Api/Pizza")]
    public class PizzaController : ApiController
    {
        readonly IPizzaFunctions _pizzaFunctions;

        public PizzaController()
        {
            _pizzaFunctions = new PizzaFunctions();
        }

        [Route("{id}/{translate}")]
        public IHttpActionResult Get(string id, string translate)
        {
            try
            {
                return Ok(_pizzaFunctions.GetPizza(id, translate));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

        }

        [Route("PizzaList/{translate}")]
        public IHttpActionResult GetPizzas(string translate)
        {
            try
            {
                return Ok(_pizzaFunctions.GetPizzas(translate));

            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

        }
        public IHttpActionResult Index()
        {
            return Ok();
        }
    }
}
