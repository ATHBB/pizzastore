﻿namespace PizzaStore.DAL.DBProperties
{
    public static class DBProperties
    {
        public const string ConnectionString = "mongodb://localhost:27017";
        public const string DatabaseName = "PizzaStore";
    }
}
