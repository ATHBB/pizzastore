﻿using MongoDB.Driver;
using System.Linq;

namespace PizzaStore.DAL.DataAccess
{
    public class DataAccess<T> : IDataAccess<T> where T : class
    {
        readonly IMongoCollection<T> _collection;

        public DataAccess()
        {
            var _client = new MongoClient(DBProperties.DBProperties.ConnectionString);
            var _database = _client.GetDatabase(DBProperties.DBProperties.DatabaseName);
            _collection = _database.GetCollection<T>(typeof(T).Name.ToLower());
        }

        public IQueryable<T> Collection
        {
            get
            {
                return _collection.AsQueryable();
            }
        }
    }
}
