﻿using System.Linq;

namespace PizzaStore.DAL.DataAccess
{
    public interface IDataAccess<T> where T : class
    {
        IQueryable<T> Collection { get; }
    }
}