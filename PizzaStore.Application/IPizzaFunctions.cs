﻿using PizzaStore.Application.VModels;
using PizzaStore.DAL.Models;
using System.Collections.Generic;

namespace PizzaStore.Application
{
    public interface IPizzaFunctions
    {
        PizzaVM GetPizza(string id, string translate);
        List<PizzaVM> GetPizzas(string translate);
    }
}