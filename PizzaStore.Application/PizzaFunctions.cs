﻿using PizzaStore.Application.VModels;
using PizzaStore.DAL.Models;
using PizzaStore.Responsobilty.Successors;
using PizzaStore.Services.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PizzaStore.Application
{
    public class PizzaFunctions : IPizzaFunctions
    {
        readonly ByCodeSuccessor _byCodeTranslate;
        readonly IPizzaService _pizzaService;

        public PizzaFunctions()
        {
            _byCodeTranslate = new ByCodeSuccessor();
            _pizzaService = new PizzaService();
            SetSuccessors();
        }

        void SetSuccessors()
        {
            var _engTranslate = new ENGSuccessor();
            var _plTranslate = new PLSuccessor();
            var _anyTranslate = new AnySuccessor();
            _byCodeTranslate.SetSuccessor(_engTranslate);
            _engTranslate.SetSuccessor(_plTranslate);
            _plTranslate.SetSuccessor(_anyTranslate);
        }

        public PizzaVM GetPizza(string id, string translate)
        {
            var pizza = _pizzaService.GetPizzaById(id);
            if (pizza != null)
            {
                var pizzaWithTranslate = _byCodeTranslate.GetPizza(pizza, translate);
                return new PizzaVM { Name = pizzaWithTranslate.Contents.FirstOrDefault().Name, Price = pizzaWithTranslate.Price };
            }
               
            throw new Exception("nie ma pizzy");
        }

        public List<PizzaVM> GetPizzas(string translate)
        {
            var pizzas = _pizzaService.GetPizzas();
            if ((bool)pizzas?.Any())
                return _byCodeTranslate.GetPizzas(pizzas, translate).Select(x=> new PizzaVM { Price = x.Price, Name = x.Contents.FirstOrDefault().Name }).ToList();
            throw new Exception("nie ma pizzy");
        }

    }
}
