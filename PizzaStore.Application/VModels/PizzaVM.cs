﻿namespace PizzaStore.Application.VModels
{
    public class PizzaVM
    {
        public double Price { get; set; }
        public string Name { get; set; }
    }
}
